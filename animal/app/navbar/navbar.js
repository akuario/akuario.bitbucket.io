angular.module("app").controller("navController", function($scope) {
  info("Nav loaded");
  $scope.activeBar = false;

  if (window.innerWidth >= 768) {
    $scope.enterBar = () => {
      $scope.activeBar = true;
      _("nav").add("active-bar");
    };

    $scope.leaveBar = () => {
      $scope.activeBar = false;
      _("nav").del("active-bar");
    };
  }
});
