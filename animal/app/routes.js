app.config(($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) => {
  $ocLazyLoadProvider.config({
    debug: false,
    events: false
  });

  $urlRouterProvider.otherwise("/flyer");

  $stateProvider.state("flyer", {
    name: "flyer",
    url: "/flyer",
    controller: "flyerController",
    templateUrl: "./app/flyer/flyer.html",
    resolve: {
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/flyer.css"
          },
          {
            href: "./assets/css/ui-cropper.css"
          },
          {
            href: "./assets/css/m.flyer.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      loadCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/flyer.js");
      }
    }
  });

  $stateProvider.state("cards", {
    name: "cards",
    url: "/cards",
    controller: "cardsController",
    templateUrl: "./app/cards/cards.html",
    resolve: {
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/cards.css"
          },
          {
            href: "./assets/css/m.cards.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      loadCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/cards.js");
      }
    }
  });
});
