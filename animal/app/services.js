app.factory("pdf", () => {
  let toMM = 2.8346;
  const mm = size => size * toMM;

  let pdf = {};
  pdf.mm = size => size * toMM;

  pdf.size = {
    portrait: {
      A0: [mm(864.28), mm(1212.28)],
      A1: [mm(617.28), mm(864.28)],
      A2: [mm(443.28), mm(617.28)],
      SRA3: [mm(343.28), mm(473.28)],
      A3: [mm(320.28), mm(443.28)],
      A4: [mm(233.28), mm(320.28)],
      A5: [mm(171.28), mm(233.28)],
      A6: [mm(128.28), mm(171.28)],
      DL: [mm(123.28), mm(223.28)],
      card: [mm(78.28), mm(108.28)]
    },
    landscape: {
      A0: [mm(1212.28), mm(864.28)],
      A1: [mm(864.28), mm(617.28)],
      A2: [mm(617.28), mm(443.28)],
      SRA3: [mm(473.28), mm(343.28)],
      A3: [mm(443.28), mm(320.28)],
      A4: [mm(320.28), mm(233.28)],
      A5: [mm(233.28), mm(171.28)],
      A6: [mm(171.28), mm(128.28)],
      DL: [mm(223.28), mm(123.28)],
      card: [mm(108.28), mm(78.28)]
    },
    bookmark: [mm(73.28), mm(233.28)],
    diptic: [mm(233.28), mm(233.28)],
    squareCard: [mm(78.28), mm(78.28)]
  };

  pdf.bleeds = {
    portrait: {
      A0: "./assets/img/bleeds/portrait-A0.svg",
      A1: "./assets/img/bleeds/portrait-A1.svg",
      A2: "./assets/img/bleeds/portrait-A2.svg",
      SRA3: "./assets/img/bleeds/portrait-SRA3.svg",
      A3: "./assets/img/bleeds/portrait-A3.svg",
      A4: "./assets/img/bleeds/portrait-A4.svg",
      A5: "./assets/img/bleeds/portrait-A5.svg",
      A6: "./assets/img/bleeds/portrait-A6.svg",
      DL: "./assets/img/bleeds/portrait-DL.svg",
      card: "./assets/img/bleeds/portrait-card.svg"
    },
    landscape: {
      A0: "./assets/img/bleeds/landscape-A0.svg",
      A1: "./assets/img/bleeds/landscape-A1.svg",
      A2: "./assets/img/bleeds/landscape-A2.svg",
      SRA3: "./assets/img/bleeds/landscape-SRA3.svg",
      A3: "./assets/img/bleeds/landscape-A3.svg",
      A4: "./assets/img/bleeds/landscape-A4.svg",
      A5: "./assets/img/bleeds/landscape-A5.svg",
      A6: "./assets/img/bleeds/landscape-A6.svg",
      DL: "./assets/img/bleeds/landscape-DL.svg",
      card: "./assets/img/bleeds/landscape-card.svg"
    },
    bookmark: "./assets/img/bleeds/bookmark.svg",
    diptic: "./assets/img/bleeds/diptic.svg",
    squareCard: "./assets/img/bleeds/square-card.svg"
  };

  pdf.margin = {
    big: mm(11.64),
    small: mm(8.64)
  };

  return pdf;
});