angular.module("app").controller("homeController", function($scope, $http, pdf) {
  info("Card loaded");

  $scope.data = {
    list: '· Comunicacion animal\n· Acupuntura / Shiatsu / Mtc\n· Nutricion natural\n· Etologia canina/felina/equina\n· "Daoma" el camino a traves del caballo\n· Doula del alma / acompañamiento transitos a la muerte\n· Educacion en positivo\n· Coaching y pedagogia holistica\n· Talleres y charlas \n· Residencias / canguros',
    contact: {
      name: "Ruth Corominas",
      phone: "(0034) 666 603 942"
    },
    contact2: {
      name: "Marga Marruecos",
      phone: "(0034) 653 453 421"
    }
  };

  if (window.innerWidth <= 768) {
    $scope.isMobile = true;
  } else {
    $scope.isMobile = false;
  }

  $scope.autoGrow = $event => {
    $event.currentTarget.style.height = "5px";
    $event.currentTarget.style.height = $event.currentTarget.scrollHeight + "px";
  };

  $scope.pushPDF = () => {
    let layout = document.querySelector("iframe");
    let mm = num => pdf.mm(num);
    let doc = new PDFDocument({
      size: pdf.size.bookmark,
      margin: pdf.margin.small,
      info: {
        Title: $scope.data.fullname + "_ANIMAL-BOOKMARK"
      }
    });
    let stream = doc.pipe(blobStream());

    // preload files
    async function asyncData() {
      // fonts
      const bold = await $http.get("./assets/font/CircularStd-Bold.ttf", { responseType: "arraybuffer" });
      const book = await $http.get("./assets/font/CircularStd-Book.ttf", { responseType: "arraybuffer" });
      doc.registerFont("bold", bold.data);
      doc.registerFont("book", book.data);

      // bleeds
      $scope.bleeds = await $http.get(pdf.bleeds.bookmark);

      // images & vectors
      $scope.bgFront = await $http.get("./assets/img/bookmark-front-bg.jpg", { responseType: "arraybuffer" });
      $scope.logoFront = await $http.get("./assets/img/logo-animal-vertical.svg");

      $scope.bgBack = await $http.get("./assets/img/bookmark-back-bg.jpg", { responseType: "arraybuffer" });
      $scope.bodyBack = await $http.get("./assets/img/bookmark-back-body.svg");
    }

    // design pdf
    asyncData().then(() => {
      doc.image($scope.bgBack.data, {
        width: mm(56),
        height: mm(216)
      });
      doc.addSVG($scope.bodyBack.data, pdf.margin.small, pdf.margin.small, {
        width: mm(56),
        height: mm(216)
      });
      
      doc
        .font("book", 8)
        .fillColor("#fff")
        .text($scope.data.list || "", pdf.margin.small + mm(3 + 4), mm(107), {
          width: mm(42),
          height: mm(55)
        });

      doc
        .font("book", 9)
        .fillColor("#fff")
        .text($scope.data.contact.name || "", pdf.margin.small + mm(3 + 4), mm(175), {
          width: mm(42),
          align: "center"
        });
      doc
        .font("bold", 9)
        .fillColor("#fff")
        .text($scope.data.contact.phone || "", pdf.margin.small + mm(3 + 4), mm(179), {
          width: mm(42),
          align: "center"
        });

      doc
        .font("book", 9)
        .fillColor("#fff")
        .text($scope.data.contact2.name || "", pdf.margin.small + mm(3 + 4), mm(188), {
          width: mm(42),
          align: "center"
        });
      doc
        .font("bold", 9)
        .fillColor("#fff")
        .text($scope.data.contact2.phone || "", pdf.margin.small + mm(3 + 4), mm(192), {
          width: mm(42),
          align: "center"
        });

      doc.addSVG($scope.bleeds.data, 0, 0);

      doc.addPage();

      doc.image($scope.bgFront.data, {
        width: mm(56),
        height: mm(216)
      });

      doc.addSVG($scope.logoFront.data, pdf.margin.small, mm(165), {
        width: mm(56),
        height: mm(50)
      });

      doc.addSVG($scope.bleeds.data, 0, 0);

      // render pdf
      doc.end();
    });

    // get pdf
    stream.on("finish", function() {
      let blob = stream.toBlobURL("application/pdf");
      layout.src = blob;
      _("#link").setAttribute("href", blob);
    });
  };

  $scope.pushPDF();

  $scope.$watch(
    "data",
    function() {
      $scope.pushPDF();
    },
    true
  );
});


//# sourceMappingURL=home.js.map