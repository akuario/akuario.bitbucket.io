angular
  .module("app")
  .controller("flyerController", function($scope, $http, $timeout, pdf) {
    info("Flyer loaded");

    if (localStorage.animalFlyer === undefined) {
      $scope.data = {
        logo: "white",
        bgColor: "#111",
        priColor: "#111",
        textColor: "#fff",
        textColorSec: "#fff",
        font: "Circular",
        title: "Animal Holistic\nJourney",
        imgFront: "./assets/img/animal-img-front.jpg",
        list: {
          titleLeft: "Actividades",
          listLeft:
            '· Comunicacion animal\n· Acupuntura / Shiatsu / Mtc\n· Nutricion natural\n· Etologia canina/felina/equina\n· "Daoma" el camino a traves del caballo',
          titleRight: "Actividades",
          listRight:
            "· Doula del alma / acompañamiento transitos a la muerte\n· Educacion en positivo\n· Coaching y pedagogia holistica\n· Talleres y charlas \n· Residencias / canguros"
        },
        event: "28 Octubre, de 10 a 14h. Precio 90€",
        contact:
          "C/ Emili Cabanyes, 20 Mataró. Centro de Yoga Shanti\nwww.terapianaturalanimal.org\n653 453 421 / 696 122 364",
        fb: true,
        imgBack: "./assets/img/animal-img-back.jpg",
        descTitle: "¿En qué consiste el taller?",
        desc:
          "Con el objetivo de avanzar en nuestra comunicacion con los animales y nuestro entorno nace Aninal Holistic Journey un proyecto holistico multidisciplinar y un espacio de unidad y conciencia donde diferentes profesionales acompañan y dan a conocer herramientas y servicios que proporcionan una mejoria en la relacion humano-animal desde la responsabilidad en uno mismo, el respeto y el amor."
      };
    } else {
      $scope.data = JSON.parse(localStorage.getItem("animalFlyer"));
    }

    if (window.innerWidth <= 768) {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    $scope.autoGrow = $event => {
      $event.currentTarget.style.height = "5px";
      $event.currentTarget.style.height =
        $event.currentTarget.scrollHeight + "px";
    };

    $scope.imgOne = "";
    $scope.cropOne = "";
    $scope.imgTwo = "";
    $scope.cropTwo = "";

    let inputFileOne = document.getElementById("imgOne");
    let inputFileTwo = document.getElementById("imgTwo");

    let handleFileSelectOne = e => {
      var file = e.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = e => {
        $scope.$apply($scope => {
          $scope.imgOne = e.target.result;
          $timeout(() => {
            $scope.data.imgFront = $scope.cropOne;
            $scope.pushPDF();
          }, 1000);
        });
      };
      reader.readAsDataURL(file);
    };
    inputFileOne.addEventListener("change", handleFileSelectOne);

    $scope.resizeOne = () => {
      $timeout(() => {
        $scope.data.imgFront = $scope.cropOne;
        $scope.pushPDF();
      }, 1000);
    };

    let handleFileSelectTwo = e => {
      var file = e.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = e => {
        $scope.$apply($scope => {
          $scope.imgTwo = e.target.result;
          $timeout(() => {
            $scope.data.imgBack = $scope.cropTwo;
            $scope.pushPDF();
          }, 1000);
        });
      };
      reader.readAsDataURL(file);
    };
    inputFileTwo.addEventListener("change", handleFileSelectTwo);

    $scope.resizeTwo = () => {
      $timeout(() => {
        $scope.data.imgBack = $scope.cropTwo;
        $scope.pushPDF();
      }, 1000);
    };

    $scope.pushPDF = () => {
      let layout = document.querySelector("iframe");
      let mm = num => pdf.mm(num);
      let doc = new PDFDocument({
        size: pdf.size.portrait.A5,
        margin: pdf.margin.small,
        info: {
          Title: $scope.data.title + "_MARGA-FLYER"
        }
      });
      let stream = doc.pipe(blobStream());

      // preload files
      async function asyncData() {
        // fonts
        const book = await $http.get("./assets/font/CircularStd-Book.ttf", {
          responseType: "arraybuffer"
        });
        doc.registerFont("book", book.data);

        if ($scope.data.font === "Circular") {
          const bold = await $http.get("./assets/font/CircularStd-Bold.ttf", {
            responseType: "arraybuffer"
          });
          doc.registerFont("bold", bold.data);
        }
        if ($scope.data.font === "Times") {
          const bold = await $http.get("./assets/font/Times-Bold.ttf", {
            responseType: "arraybuffer"
          });
          doc.registerFont("bold", bold.data);
        }
        if ($scope.data.font === "Neue") {
          const bold = await $http.get("./assets/font/Helvetica-UL.otf", {
            responseType: "arraybuffer"
          });
          doc.registerFont("bold", bold.data);
        }
        if ($scope.data.font === "Dancing") {
          const bold = await $http.get("./assets/font/Dancing-Script.otf", {
            responseType: "arraybuffer"
          });
          doc.registerFont("bold", bold.data);
        }

        // bleeds
        $scope.bleeds = await $http.get(pdf.bleeds.portrait.A5);

        // images & vectors
        $scope.logo = await $http.get(
          `./assets/img/animal-${$scope.data.logo}.svg`
        );
        $scope.imgFront = await $http.get($scope.data.imgFront, {
          responseType: "arraybuffer"
        });
        $scope.fb = await $http.get(`./assets/img/facebook.svg`);
        $scope.imgBack = await $http.get($scope.data.imgBack, {
          responseType: "arraybuffer"
        });
      }

      // design pdf
      asyncData().then(() => {
        doc
          .rect(pdf.margin.small, pdf.margin.small, mm(154), mm(216))
          .fill($scope.data.bgColor);
        doc
          .rect(pdf.margin.small, pdf.margin.small, mm(154), mm(5))
          .fill($scope.data.priColor);

        doc
          .save()
          .rect(pdf.margin.small, pdf.margin.small + mm(42), mm(154), mm(86))
          .clip()
          .image(
            $scope.imgFront.data,
            pdf.margin.small,
            pdf.margin.small + mm(42),
            {
              width: mm(154)
            }
          )
          .restore();

        /* let grad = doc
        .linearGradient(pdf.margin.small, pdf.margin.small + mm(42), mm(154), mm(20))
        .stop(0, $scope.data.bgColor, 1)
        .stop(1, $scope.data.bgColor, 0);

      doc
      .save()
      .rect(pdf.margin.small, pdf.margin.small + mm(42), mm(154), mm(20))
      .fill(grad)
      .restore(); */

        doc.addSVG(
          $scope.logo.data,
          pdf.margin.small + mm(8),
          pdf.margin.small + mm(8),
          {
            width: mm(40),
            height: mm(30)
          }
        );

        doc
          .font("bold", 22)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.title || "",
            pdf.margin.small + mm(8 + 50),
            pdf.margin.small + mm(8),
            {
              width: mm(88),
              height: mm(30)
            }
          );

        doc
          .font("bold", 16)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.list.titleLeft || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(136),
            {
              width: mm(64)
            }
          );

        doc
          .rect(
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(145),
            mm(64),
            0.5
          )
          .fill($scope.data.textColor);
        doc
          .rect(
            pdf.margin.small + mm(8 + 74),
            pdf.margin.small + mm(145),
            mm(64),
            0.5
          )
          .fill($scope.data.textColor);

        doc
          .font("bold", 16)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.list.titleRight || "",
            pdf.margin.small + mm(8 + 75),
            pdf.margin.small + mm(136),
            {
              width: mm(64)
            }
          );

        doc
          .font("book", 10)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.list.listLeft || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(148),
            {
              width: mm(64),
              height: mm(40)
            }
          );

        doc
          .font("book", 10)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.list.listRight || "",
            pdf.margin.small + mm(8 + 75),
            pdf.margin.small + mm(148),
            {
              width: mm(64),
              height: mm(40)
            }
          );

        doc
          .rect(
            pdf.margin.small,
            pdf.margin.small + mm(216 - 35),
            mm(154),
            mm(35)
          )
          .fill($scope.data.priColor);

        doc
          .font("bold", 16)
          .fillColor($scope.data.textColorSec)
          .text(
            $scope.data.event || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(185),
            {
              width: mm(138),
              align: "center"
            }
          );

        doc
          .font("book", 10)
          .fillColor($scope.data.textColorSec)
          .text(
            $scope.data.contact || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(194),
            {
              width: mm(138),
              align: "center"
            }
          );

        if ($scope.data.fb === true) {
          doc.addSVG(
            $scope.fb.data,
            pdf.margin.small + mm(140),
            pdf.margin.small + mm(202),
            {
              width: mm(7),
              height: mm(7)
            }
          );
        }

        doc.addSVG($scope.bleeds.data, 0, 0);

        doc.addPage();

        doc
          .rect(pdf.margin.small, pdf.margin.small, mm(154), mm(216))
          .fill($scope.data.bgColor);
        doc
          .rect(pdf.margin.small, pdf.margin.small, mm(154), mm(5))
          .fill($scope.data.priColor);

        doc
          .save()
          .rect(pdf.margin.small, pdf.margin.small + mm(42), mm(154), mm(104))
          .clip()
          .image(
            $scope.imgBack.data,
            pdf.margin.small,
            pdf.margin.small + mm(42),
            {
              width: mm(154)
            }
          )
          .restore();

        doc.addSVG(
          $scope.logo.data,
          pdf.margin.small + mm(8),
          pdf.margin.small + mm(8),
          {
            width: mm(40),
            height: mm(30)
          }
        );

        doc
          .font("bold", 22)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.title || "",
            pdf.margin.small + mm(8 + 50),
            pdf.margin.small + mm(8),
            {
              width: mm(88),
              height: mm(30)
            }
          );

        doc
          .font("bold", 12)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.descTitle || "",
            pdf.margin.small + mm(8 + 5),
            mm(160),
            {
              width: mm(128)
            }
          );

        doc
          .font("book", 10)
          .fillColor($scope.data.textColor)
          .text($scope.data.desc || "", pdf.margin.small + mm(10), mm(170), {
            width: mm(134),
            height: mm(60)
          });

        doc
          .rect(pdf.margin.small, pdf.margin.small + mm(189), mm(154), mm(27))
          .fill($scope.data.priColor);

        doc
          .font("book", 10)
          .fillColor($scope.data.textColorSec)
          .text(
            $scope.data.contact || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(194),
            {
              width: mm(138),
              align: "center"
            }
          );

        if ($scope.data.fb === true) {
          doc.addSVG(
            $scope.fb.data,
            pdf.margin.small + mm(140),
            pdf.margin.small + mm(202),
            {
              width: mm(7),
              height: mm(7)
            }
          );
        }

        doc.addSVG($scope.bleeds.data, 0, 0);

        // render pdf
        doc.end();
      });

      // get pdf
      stream.on("finish", function() {
        let blob = stream.toBlobURL("application/pdf");
        layout.src = blob;
        _("#link").setAttribute("href", blob);
      });
    };

    $scope.pushPDF();

    $scope.$watch(
      "data",
      function() {
        $scope.pushPDF();
        localStorage.setItem("animalFlyer", JSON.stringify($scope.data));
      },
      true
    );
  });


//# sourceMappingURL=flyer.js.map