angular
  .module("app")
  .controller("cardsController", function($scope, $http, pdf) {
    info("Card loaded");

    if (localStorage.margaCards === undefined) {
      $scope.data = {
        logo: "black",
        bgColor: "#f2f2f2",
        elsColor: "green",
        textColor: "#000",
        font: "Circular",
        title: "TERAPEUTA EDUCADORA",
        list:
          "Medicina Tradicional China\nAcupuntura\nMoxibustión\nFitoterapia\nHomeopatía\nFlores de Bach",
        desc:
          "Calidad humano profecional al servicio de tu animal Etología, flores de bach y acupuntura en animales",
        phone: "653 453 421",
        fb: "Marga Marruecos"
      };
    } else {
      $scope.data = JSON.parse(localStorage.getItem("margaCards"));
    }

    if (window.innerWidth <= 768) {
      $scope.isMobile = true;
    } else {
      $scope.isMobile = false;
    }

    $scope.pushPDF = () => {
      let layout = document.querySelector("iframe");
      let mm = num => pdf.mm(num);
      let doc = new PDFDocument({
        size: pdf.size.landscape.card,
        margin: pdf.margin.small,
        info: {
          Title: $scope.data.title + "_MARGA-targetas"
        }
      });

      let stream = doc.pipe(blobStream());

      // preload files
      async function asyncData() {
        // fonts
        const book = await $http.get("./assets/font/CircularStd-Book.ttf", {
          responseType: "arraybuffer"
        });
        doc.registerFont("book", book.data);

        const neue = await $http.get("./assets/font/Helvetica-UL.otf", {
          responseType: "arraybuffer"
        });
        doc.registerFont("neue", neue.data);

        if ($scope.data.font === "Circular") {
          const bold = await $http.get("./assets/font/CircularStd-Bold.ttf", {
            responseType: "arraybuffer"
          });
          doc.registerFont("bold", bold.data);
        }
        if ($scope.data.font === "Times") {
          const bold = await $http.get("./assets/font/Times-Bold.ttf", {
            responseType: "arraybuffer"
          });
          doc.registerFont("bold", bold.data);
        }
        if ($scope.data.font === "Neue") {
          const bold = await $http.get("./assets/font/Helvetica-UL.otf", {
            responseType: "arraybuffer"
          });
          doc.registerFont("bold", bold.data);
        }
        if ($scope.data.font === "Dancing") {
          const bold = await $http.get("./assets/font/Dancing-Script.otf", {
            responseType: "arraybuffer"
          });
          doc.registerFont("bold", bold.data);
        }

        // bleeds
        $scope.bleeds = await $http.get(pdf.bleeds.landscape.card);

        // images & vectors
        $scope.logo = await $http.get(
          `./assets/img/marga-logo-${$scope.data.logo}.svg`
        );
        $scope.els = await $http.get(
          `./assets/img/card${$scope.data.elsColor}.svg`
        );
      }

      // design pdf
      asyncData().then(() => {
        doc
          .rect(pdf.margin.small, pdf.margin.small, mm(91), mm(61))
          .fill($scope.data.bgColor);

        doc.addSVG(
          $scope.logo.data,
          pdf.margin.small,
          pdf.margin.small + mm(15),
          {
            width: mm(91),
            height: mm(30)
          }
        );

        doc
          .font("bold", 10)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.title || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(48),
            {
              width: mm(75),
              align: "center",
              characterSpacing: 2
            }
          );

        doc.addSVG($scope.bleeds.data, 0, 0);

        doc.addPage();

        doc
          .rect(pdf.margin.small, pdf.margin.small, mm(91), mm(61))
          .fill($scope.data.bgColor);

        doc.addSVG($scope.els.data, pdf.margin.small, pdf.margin.small, {
          width: mm(91),
          height: mm(61)
        });

        doc
          .font("book", 9)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.list || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(8),
            {
              width: mm(60)
            }
          );

        doc
          .font("book", 9)
          .fillColor($scope.data.textColor)
          .text($scope.data.desc || "", pdf.margin.small + mm(8), mm(45), {
            width: mm(75)
          });

        doc
          .font("book", 9)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.phone || "",
            pdf.margin.small + mm(11),
            pdf.margin.small + mm(49.5),
            {
              width: mm(30)
            }
          );

        doc
          .font("book", 9)
          .fillColor($scope.data.textColor)
          .text(
            $scope.data.fb || "",
            pdf.margin.small + mm(35),
            pdf.margin.small + mm(49.5),
            {
              width: mm(48)
            }
          );

        doc.addSVG($scope.bleeds.data, 0, 0);

        // render pdf
        doc.end();
      });

      // get pdf
      stream.on("finish", function() {
        let blob = stream.toBlobURL("application/pdf");
        layout.src = blob;
        _("#link").setAttribute("href", blob);
      });
    };

    $scope.pushPDF();

    $scope.$watch(
      "data",
      function() {
        $scope.pushPDF();
        localStorage.setItem("margaCards", JSON.stringify($scope.data));
      },
      true
    );
  });
