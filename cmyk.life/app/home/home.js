angular
  .module("app")
  .controller("homeController", function($scope, $timeout, $interval) {
    info("Home loaded");

    let wH = window.innerHeight;
    window.addEventListener("resize", () => {
      wH = window.innerHeight;
    });

    _(".home-box").on("scroll", () => {
      if (_(".home-box").scrollTop >= 400) {
        _(".info").add("hide");
      } else {
        _(".info").del("hide");
      }

      if (_(".home-box").scrollTop > 400) {
        _(".number").del("hide");
      } else {
        _(".number").add("hide");
      }
      if (_(".home-box").scrollTop >= wH + 100) {
        _(".number").add("hide");
      }
    });

    $scope.goTop = () => {
      _(".home-box").scrollTo({
        behavior: "smooth",
        left: 0,
        top: 0
      });
    };

    $scope.goContact = () => {
      _(".home-box").scrollTo({
        behavior: "smooth",
        left: 0,
        top: wH * 2
      });
    };

    let sentencesList = [
      "¿Crees que tu vida profesional debería ir de la mano de tu vida personal?",
      "¿Crees en la casualidad, o visualizas una causalidad?",
      "¿Crees que divertirte en tu trabajo es sinonimo de más productividad?",
      "¿Crees que todos tenemos unas características especiales que nos realizan?",
      "¿Crees que potenciar estas características sirve para marcar la diferencia?",
      "¿Intuyes o sientes que tienes un propósito?",
      "¿Te gustaría compartir la vida con gente que la ve del mismo color que tu?"
    ];

    let inc = 1;
    $scope.total = sentencesList.length;
    $scope.actual = 1;
    $scope.display = sentencesList[inc - 1];

    $scope.goNext = () => {
      if (inc <= $scope.total - 1) {
        $scope.display = sentencesList[++inc - 1];
        ++$scope.actual;
      } else {
        _(".home-box").scrollTo({
          behavior: "smooth",
          left: 0,
          top: wH * 2
        });
      }
    };

    $scope.sendMail = form => {
      window.location.href = `mailto:hola@cmyk.life?subject=${form.name}, ${
        form.contact
      }&body=${form.body}`;
    };

    let tic = new Audio("./assets/img/tic.mp3");
    let toc = new Audio("./assets/img/toc.mp3");
    let success = new Audio("./assets/img/success.mp3");

    let easterEgg = () => {
      $scope.goCoherent = () => {};
      log("Playing coherence...");

      let ticPlay;
      let tocPlay;
      let timer;

      _("#r").add("show");
      _(".logo").add("hide");

      let time = 60 * 5,
        r = document.getElementById("r"),
        tmp = time;

      timer = $interval(() => {
        let c = tmp--,
          m = (c / 60) >> 0,
          s = c - m * 60 + "";
        r.textContent = m + ":" + (s.length > 1 ? "" : "0") + s;
        tmp != 0 || (tmp = time);
      }, 1000);

      tic.play();

      ticPlay = $interval(() => {
        tic.play();
      }, 12000);

      $timeout(() => {
        toc.play();

        tocPlay = $interval(() => {
          toc.play();
        }, 12000);
      }, 6000);

      $timeout(() => {
        $interval.cancel(ticPlay);
        $interval.cancel(tocPlay);
        $interval.cancel(timer);
        success.play();
        _("#r").del("show");
        _(".logo").del("hide");
        $scope.goCoherent = easterEgg;
      }, 1000 * (60 * 5));
    };

    $scope.goCoherent = easterEgg;
  });
