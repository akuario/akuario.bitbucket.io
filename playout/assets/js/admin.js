angular
  .module("app")
  .controller("adminController", function(
    $scope,
    server,
    $timeout,
    $stateParams,
    $state,
    toaster,
    $http,
    pdf
  ) {
    info("Admin loaded");

    let wW = window.innerWidth;
    if (wW <= 768) {
      $scope.mov = false;
    } else {
      $scope.mov = true;
    }

    $scope.getDb = $stateParams.db;
    $scope.getType = $stateParams.type;
    $scope.search = "";

    $scope.goTo = state => {
      $state.go(state, { db: $scope.getDb, data: null, from: $scope.getType });
    };

    $scope.openMenu = () => {
      _("nav").add("open");
    };

    $scope.activeSearch = false;
    $scope.turnSearch = () => {
      $scope.activeSearch = !$scope.activeSearch;
      $timeout(() => _("#search").focus());
    };

    $scope.noFocus = () => {
      if ($scope.search.length === 0) $scope.activeSearch = false;
    };

    $scope.stateReload = () => $state.reload();

    //get data from active database
    server.db
      .collection($scope.getDb)
      .get()
      .then(snap => {
        let data = [];
        snap.forEach(doc => {
          let values = doc.data();
          values.id = doc.id;
          data.push(values);
        });
        $scope.$apply(() => {
          $scope.cards = data;
          succ(`${$scope.getDb.toUpperCase()} data obtained`);
        });
      });

    //go edit data
    $scope.getId = id => {
      $state.go("form", { db: $scope.getDb, data: id, from: $scope.getType });
    };

    //remove from database
    $scope.removeData = card => {
      let sure = confirm("¿Borrar permanentemente?");
      if (sure === true) {
        server.db
          .collection($scope.getDb)
          .doc(card.id)
          .delete()
          .then(() => {
            succ("Document successfully deleted");

            if (card.img) {
              let imgRef = server.st.ref().child(`${$scope.getDb}/${card.id}`);

              imgRef
                .delete()
                .then(() => {
                  succ("Image successfully deleted");
                  toaster("Document deleted", 2000);
                })
                .catch(err => {
                  warn(`Error removing image: ${err}`);
                });
            }

            $state.reload();
          })
          .catch(err => {
            warn(`Error removing document: ${err}`);
          });
      }
    };

    //get this year
    let d = new Date();
    let y = d.getFullYear();
    let yy = y.toString().substring(2);
    let tempName;
    let pdf_date;

    //gestion template
    $scope.gestionPDF = data => {
      info("PDF starts");
      _(".loading").add("loading-active");

      tempName = `${yy}-${data.num.pad(3)}_${data.reference}_${data.type}`;
      let frame = document.querySelector("iframe");
      let mm = num => pdf.mm(num);
      let doc = new PDFDocument({
        size: [mm(210), mm(297)],
        margin: mm(0),
        info: {
          Title: data.reference
        }
      });
      let stream = doc.pipe(blobStream());

      // preload files
      async function asyncData() {
        // fonts
        const fira = await $http.get("./assets/font/Fira-Code.ttf", {
          responseType: "arraybuffer"
        });

        doc.registerFont("fira", fira.data);

        // images & vectors
        $scope.ppto7 = await $http.get("./assets/img/templates/ppto-7.svg");
        $scope.bill7 = await $http.get("./assets/img/templates/bill-7.svg");
        $scope.ppto15 = await $http.get("./assets/img/templates/ppto-15.svg");
        $scope.bill15 = await $http.get("./assets/img/templates/bill-15.svg");

        //convert date to human
        pdf_date = await new Date(data.date.seconds * 1000);
      }

      // design pdf
      asyncData().then(() => {
        let pdf_y = pdf_date.getFullYear();
        let pdf_yy = pdf_y.toString().substring(2);
        let pdf_m = pdf_date.getMonth() + 1;
        let pdf_d = pdf_date.getDate();

        //template
        doc.addSVG(eval(`$scope.${data.type}${data.ret}.data`), 0, 0);

        //num & date
        doc.font("fira", 7).text(`${yy}-${data.num.pad(3)}`, mm(168), mm(28.9));
        doc
          .font("fira", 7)
          .text(`${pdf_d}-${pdf_m}-${pdf_yy}`, mm(168), mm(33.9));

        //reference
        doc.font("fira", 7).text(data.reference, mm(42.8), mm(49.2));

        //customer data
        doc.font("fira", 7).text(data.to, mm(32), mm(64.5));
        doc.font("fira", 7).text(data.nif, mm(159), mm(64.5));
        doc.font("fira", 7).text(data.address, mm(39), mm(71.7));
        doc.font("fira", 7).text(data.contact, mm(164), mm(71.7));

        //to invoice
        let basePrice = 0;

        for (let i = 0; i < data.to_invoice.length; i++) {
          doc
            .font("fira", 7)
            .text(data.to_invoice[i].quantity, mm(27), mm(95 + i * 5.3));
          doc
            .font("fira", 7)
            .text(data.to_invoice[i].detail, mm(38), mm(95 + i * 5.3));
          doc
            .font("fira", 7)
            .text(
              data.to_invoice[i].discount + "%" === "0%"
                ? ""
                : data.to_invoice[i].discount + "%",
              mm(139),
              mm(95 + i * 5.3)
            );
          doc
            .font("fira", 7)
            .text(
              data.to_invoice[i].price.toFixed(2) + "€",
              mm(150),
              mm(95 + i * 5.3)
            );
          doc
            .font("fira", 7)
            .text(
              (
                data.to_invoice[i].price * data.to_invoice[i].quantity -
                (data.to_invoice[i].price / 100) *
                  data.to_invoice[i].discount *
                  data.to_invoice[i].quantity
              ).toFixed(2) + "€",
              mm(168.5),
              mm(95 + i * 5.3)
            );

          //operations to get taxes
          basePrice +=
            data.to_invoice[i].price * data.to_invoice[i].quantity -
            (data.to_invoice[i].price / 100) *
              data.to_invoice[i].discount *
              data.to_invoice[i].quantity;
        }

        //total
        doc
          .font("fira", 7)
          .text(basePrice.toFixed(2) + "€", mm(54.5), mm(235.7));
        doc
          .font("fira", 7)
          .text(
            "-" + ((basePrice / 100) * data.ret).toFixed(2) + "€",
            mm(94),
            mm(235.7)
          );
        doc
          .font("fira", 7)
          .text(
            ((basePrice / 100) * 21).toFixed(2) + "€",
            mm(127.2),
            mm(235.7)
          );
        doc
          .font("fira", 7)
          .fillColor("#fff")
          .text(
            (
              basePrice -
              (basePrice / 100) * data.ret +
              (basePrice / 100) * 21
            ).toFixed(2) + "€",
            mm(154),
            mm(235.7)
          );

        // render pdf
        doc.end();
      });

      // get pdf
      stream.on("finish", function() {
        let blob = stream.toBlobURL("application/pdf");

        /* frame.src = blob; */

        let link = document.createElement("a");
        link.href = blob;
        link.download = `${tempName}.pdf`;
        link.click();

        $timeout(function() {
          info("PDF ends");
          _(".loading").del("loading-active");
          window.URL.revokeObjectURL(blob);
        }, 100);
      });
    };

    //empleados template
    $scope.trabajadoresPDF = data => {
      info("PDF starts");
      _(".loading").add("loading-active");
      tempName = `${data.name}_TARGETAS`;
      let frame = document.querySelector("iframe");
      let mm = num => pdf.mm(num);
      let doc = new PDFDocument({
        size: pdf.size.landscape.card,
        margin: pdf.margin.small,
        info: {
          Title: data.id + "_TARJETA_EMPLEADO"
        }
      });
      let stream = doc.pipe(blobStream());

      // preload files
      async function asyncData() {
        // fonts
        const bold = await $http.get("./assets/font/Montserrat-Bold.ttf", {
          responseType: "arraybuffer"
        });
        const book = await $http.get("./assets/font/Montserrat-Book.ttf", {
          responseType: "arraybuffer"
        });
        const chBold = await $http.get("./assets/font/NotoSansSC-Bold.otf", {
          responseType: "arraybuffer"
        });
        const chBook = await $http.get("./assets/font/NotoSansSC-Book.otf", {
          responseType: "arraybuffer"
        });
        doc.registerFont("bold", bold.data);
        doc.registerFont("book", book.data);
        doc.registerFont("chBold", chBold.data);
        doc.registerFont("chBook", chBook.data);

        // bleeds
        $scope.bleeds = await $http.get(pdf.bleeds.landscape.card);

        // images & vectors
        $scope.cara = await $http.get(
          "./assets/img/templates/tarjeta-tapasbar-cara.svg"
        );
        $scope.dorso = await $http.get(
          "./assets/img/templates/tarjeta-tapasbar-dorso.svg"
        );
      }

      // design pdf
      asyncData().then(() => {
        doc.addSVG($scope.cara.data, pdf.margin.small, pdf.margin.small, {
          width: mm(91),
          height: mm(61)
        });

        doc.addSVG($scope.bleeds.data, 0, 0);

        doc.addPage();

        doc.addSVG($scope.dorso.data, pdf.margin.small, pdf.margin.small, {
          width: mm(91),
          height: mm(61)
        });

        if (hasCh(data.name)) {
          doc
            .font("chBold", 11)
            .fillColor("#fff")
            .text(
              data.name || "",
              pdf.margin.small + mm(8),
              pdf.margin.small + mm(8),
              {
                width: mm(75)
              }
            );
        } else {
          doc
            .font("bold", 11)
            .fillColor("#fff")
            .text(
              data.name || "",
              pdf.margin.small + mm(8),
              pdf.margin.small + mm(8),
              {
                width: mm(75)
              }
            );
        }

        if (hasCh(data.position)) {
          doc
            .font("chBook", 9)
            .fillColor("#fff")
            .text(
              data.position.toUpperCase() || "",
              pdf.margin.small + mm(8),
              pdf.margin.small + mm(13),
              {
                width: mm(75)
              }
            );
        } else {
          doc
            .font("book", 9)
            .fillColor("#fff")
            .text(
              data.position.toUpperCase() || "",
              pdf.margin.small + mm(8),
              pdf.margin.small + mm(13),
              {
                width: mm(75)
              }
            );
        }

        doc
          .font("book", 9)
          .fillColor("#fff")
          .text(
            data.email || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(35),
            {
              width: mm(75)
            }
          );

        doc
          .font("book", 9)
          .fillColor("#fff")
          .text(
            data.phone || "",
            pdf.margin.small + mm(8),
            pdf.margin.small + mm(40),
            {
              width: mm(75)
            }
          );

        if (hasCh(data.address)) {
          doc
            .font("chBook", 8)
            .fillColor("#fff")
            .text(
              data.address || "",
              pdf.margin.small + mm(8),
              pdf.margin.small + mm(45),
              {
                width: mm(75)
              }
            );
        } else {
          doc
            .font("book", 9)
            .fillColor("#fff")
            .text(
              data.address || "",
              pdf.margin.small + mm(8),
              pdf.margin.small + mm(45),
              {
                width: mm(75)
              }
            );
        }

        doc.addSVG($scope.bleeds.data, 0, 0);

        // render pdf
        doc.end();
      });

      // get pdf
      stream.on("finish", function() {
        let blob = stream.toBlobURL("application/pdf");
        frame.src = blob;
        let link = document.createElement("a");
        link.href = blob;
        link.download = `${tempName}.pdf`;
        link.click();
        $timeout(function() {
          info("PDF ends");
          _(".loading").del("loading-active");
          window.URL.revokeObjectURL(blob);
        }, 100);
      });
    };
  });


//# sourceMappingURL=admin.js.map