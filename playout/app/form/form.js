angular
  .module("app")
  .controller(
    "formController",
    ($scope, server, $stateParams, $state, toaster, $timeout) => {
      info("Form loaded");

      //get router params to know database and doc
      $scope.getDb = $stateParams.db;
      $scope.getData = $stateParams.data;
      $scope.getFrom = $stateParams.from;
      $scope.form = {};

      //go back to router param
      $scope.goBack = () => {
        $state.go("admin", { db: $scope.getDb, type: $scope.getFrom });
      };

      $scope.openMenu = () => {
        _("nav").add("open");
      };

      $scope.form.to_invoice = [];
      $scope.invoice = {};

      $timeout(() => {
        $scope.sumPrice = $scope.form.to_invoice.reduce((tot, record) => {
          return (
            tot +
            record.price * record.quantity -
            (record.price / 100) * record.discount * record.quantity
          );
        }, 0);
      }, 2000);

      $scope.addInvoice = item => {
        if (item.detail && item.quantity && item.price !== undefined) {
          if (item.discount === undefined) {
            item.discount = 0;
          }

          $scope.form.to_invoice.push(item);
          $scope.invoice = null;

          $scope.sumPrice = $scope.form.to_invoice.reduce((tot, record) => {
            return (
              tot +
              record.price * record.quantity -
              (record.price / 100) * record.discount * record.quantity
            );
          }, 0);
        } else {
          _(".add-btn").tog("shake");
          $timeout(() => _(".add-btn").tog("shake"), 1000);
        }
      };

      $scope.delInvoice = key => {
        $scope.sumPrice = $scope.sumPrice - $scope.form.to_invoice[key].price;
        $scope.form.to_invoice.splice(key, 1);
      };

      //clear img in the form
      $scope.clearImg = () => ($scope.form.img = "");

      //get list of customers
      let customersRef = server.db.collection("gestion");

      customersRef.get().then(snapshot => {
        $scope.customers = [];

        snapshot.forEach(doc => {
          $scope.customers.push(doc.data());
        });

        const uniqueCustomers = Array.from(
          new Set($scope.customers.map(c => c.to))
        ).map(to => {
          return $scope.customers.find(c => c.to === to);
        });

        $scope.customers = uniqueCustomers;
      });

      //auto fill on customer list selection
      _("[list='customers']").on("change", e => {
        let autoCustomerRef = server.db
          .collection("gestion")
          .where("to", "==", e.target.value);

        autoCustomerRef.get().then(snapshot => {
          snapshot.forEach(doc => {
            $scope.autoCustomer = doc.data();
          });

          $scope.form.nif = $scope.autoCustomer.nif;
          $scope.form.address = $scope.autoCustomer.address;
          $scope.form.contact = $scope.autoCustomer.contact;
          $timeout(() => e.target.blur());
        });
      });

      //know if is open form or new from
      if ($stateParams.data !== null) {
        let selectedRef = server.db
          .collection($scope.getDb)
          .doc($scope.getData);

        //fill form with matched data
        selectedRef.get().then(doc => {
          $scope.$apply(() => {
            $scope.form = doc.data();
            $scope.form.date = new Date($scope.form.date.seconds * 1000);

            succ("Data obtained");
          });
        });

        //update form
        $scope.addForm = form => {
          let now = new Date();
          let imgRef = server.st
            .ref()
            .child(`${$scope.getDb}/${$stateParams.data}`);
          let img = form.img;

          if (typeof img !== "string") {
            let taskRef = imgRef.put(img);

            taskRef.on(
              "state_changed",
              snapshot => {
                let progress = Math.round(
                  (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );
                info(`Upload is ${progress}% done`);
                toaster(`Updating: ${progress}%`, 3000);
              },
              err => {
                warn(err);
              },
              () => {
                taskRef.snapshot.ref.getDownloadURL().then(downloadURL => {
                  succ("Image uploaded");
                  form.img = downloadURL;

                  server.db
                    .collection($scope.getDb)
                    .doc($stateParams.data)
                    .set(form)
                    .then(() => {
                      succ("Data updated");
                      $state.go("admin", {
                        db: $scope.getDb,
                        type: $scope.getFrom
                      });
                    })
                    .catch(err => {
                      warn(`Error writing document: ${err}`);
                    });
                });
              }
            );
          } else {
            server.db
              .collection($scope.getDb)
              .doc($stateParams.data)
              .set(form)
              .then(() => {
                succ("Data updated");
                $state.go("admin", { db: $scope.getDb, type: $scope.getFrom });
              })
              .catch(err => {
                warn(`Error writing document: ${err}`);
              });
          }
        };
      } else {
        //get last number depending on type
        $scope.selectType = () => {
          let typeRef = server.db
            .collection("gestion")
            .where("type", "==", $scope.form.type);

          typeRef.get().then(snapshot => {
            let lastNum = [];

            snapshot.forEach(doc => {
              lastNum.push(doc.data());
            });

            let max = Math.max.apply(
              Math,
              lastNum.map(function(o) {
                return o.num;
              })
            );
            $timeout(() => ($scope.form.num = max + 1));
          });
        };

        //create new
        $scope.addForm = form => {
          //add created data
          let now = new Date();
          form._created = now;
          //keep file
          let imgFile = form.img;
          //clear image fild to addit when gets new data
          form.img = "";
          //first add and get id
          server.db
            .collection($scope.getDb)
            .add(form)
            .then(docRef => {
              //add ref data
              server.db
                .collection($scope.getDb)
                .doc(docRef.id)
                .update({
                  ref: docRef.id
                })
                .then(() => info("Ref added"));

              succ("Added new data");
              let imgRef = server.st
                .ref()
                .child(`${$scope.getDb}/${docRef.id}`);

              //know if there is img to upload or not
              if (typeof imgFile === "object") {
                let taskRef = imgRef.put(imgFile);

                taskRef.on(
                  "state_changed",
                  snapshot => {
                    let progress = Math.round(
                      (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    );
                    info(`Upload is ${progress}% done`);
                    toaster(`Updating: ${progress}%`, 3000);
                  },
                  err => {
                    warn(err);
                  },
                  () => {
                    taskRef.snapshot.ref.getDownloadURL().then(downloadURL => {
                      succ("Image uploaded");

                      server.db
                        .collection($scope.getDb)
                        .doc(docRef.id)
                        .update({
                          img: downloadURL
                        })
                        .then(() => {
                          succ("Image added");
                          $state.go("admin", {
                            db: $scope.getDb,
                            type: $scope.getFrom
                          });
                        })
                        .catch(err => {
                          warn(`Error writing document: ${err}`);
                        });
                    });
                  }
                );
              } else {
                succ("Created new without image");
                $state.go("admin", { db: $scope.getDb, type: $scope.getFrom });
              }
            })
            .catch(err => {
              warn(`Error writing document: ${err}`);
            });
        };
      }

      //remove from database
      $scope.removeData = () => {
        let sure = confirm("¿Borrar permanentemente?");
        if (sure === true) {
          server.db
            .collection($scope.getDb)
            .doc($scope.getData)
            .delete()
            .then(() => {
              succ("Document successfully deleted");
              let imgRef = server.st
                .ref()
                .child(`${$scope.getDb}/${$scope.getData}`);

              imgRef
                .delete()
                .then(() => {
                  succ("Image successfully deleted");
                  toaster("Document deleted", 2000);
                  $state.go("admin", {
                    db: $scope.getDb,
                    type: $scope.getFrom
                  });
                })
                .catch(err => {
                  warn(`Error removing image: ${err}`);
                });
            })
            .catch(err => {
              warn(`Error removing document: ${err}`);
            });
        }
      };
    }
  );
