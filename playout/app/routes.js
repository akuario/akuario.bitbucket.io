app.config(($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) => {
  $ocLazyLoadProvider.config({
    debug: false,
    events: false
  });

  $urlRouterProvider.otherwise("/admin/gestion/bill");

  $stateProvider.state("login", {
    name: "login",
    url: "/login",
    views: {
      content: {
        templateUrl: "./app/login/login.html",
        controller: "loginController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (user) {
            $state.go("admin", { db: "gestion", type: "bill" });
          }
        });
      },
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/login.css"
          },
          {
            href: "./assets/css/m.login.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/login.js");
      }
    }
  });

  $stateProvider.state("admin", {
    name: "admin",
    url: "/admin/:db/:type",
    params: { db: null, type: null },
    views: {
      navbar: {
        templateUrl: "./app/navbar/navbar.html",
        controller: "navController"
      },
      content: {
        templateUrl: "./app/admin/admin.html",
        controller: "adminController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (!user) {
            $state.go("login");
          }
        });
      },
      navCss: $css => {
        return $css.add([
          {
            href: "./assets/css/navbar.css"
          },
          {
            href: "./assets/css/m.navbar.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      contentCss: $css => {
        return $css.add([
          {
            href: "./assets/css/admin.css"
          },
          {
            href: "./assets/css/m.admin.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      navCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/navbar.js");
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/admin.js");
      },
      gestionCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/gestion.js");
      }
    }
  });

  $stateProvider.state("form", {
    name: "form",
    url: "/form/:db/:data?from",
    params: { db: null, data: null, from: null },
    views: {
      navbar: {
        templateUrl: "./app/navbar/navbar.html",
        controller: "navController"
      },
      content: {
        templateUrl: "./app/form/form.html",
        controller: "formController"
      }
    },
    resolve: {
      redirect: (server, $state) => {
        server.auth.onAuthStateChanged(user => {
          if (!user) {
            $state.go("login");
          }
        });
      },
      navCss: $css => {
        return $css.add([
          {
            href: "./assets/css/navbar.css"
          },
          {
            href: "./assets/css/m.navbar.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      contentCss: $css => {
        return $css.add([
          {
            href: "./assets/css/form.css"
          },
          {
            href: "./assets/css/m.form.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      navCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/navbar.js");
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/form.js");
      }
    }
  });
});
