angular
  .module("app")
  .controller(
    "navController",
    ($scope, server, $state, toaster, $stateParams) => {
      info("Nav loaded");

      $scope.out = () => {
        server.out();
        $state.go("login");
        toaster("Logged out", 2000);
      };

      $scope.closeMenu = () => {
        _("nav").del("open");
      };

      $scope.getDb = $stateParams.db;
      $scope.getType = $stateParams.type;

      //get user data
      server.db
        .collection("user")
        .doc("playout")
        .get()
        .then(doc => {
          let data = doc.data();
          $scope.$apply(() => {
            $scope.user = data;
            succ("User data obtained");
          });
        });
    }
  );
