const app = angular.module("app", [
  "oc.lazyLoad",
  "ui.router",
  "angularCSS",
  "ngFileUpload",
  "ngTouch"
]);

//firebase auth
firebase.initializeApp({
  apiKey: "AIzaSyAGEHpaBX7SrOA2Gb1OBTgQJaWG-5r10lU",
  authDomain: "playout-online.firebaseapp.com",
  databaseURL: "https://playout-online.firebaseio.com",
  projectId: "playout-online",
  storageBucket: "playout-online.appspot.com",
  messagingSenderId: "422965033208"
});
