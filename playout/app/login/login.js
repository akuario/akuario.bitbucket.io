angular
  .module("app")
  .controller("loginController", ($scope, server, $state, toaster) => {
    info("Login loaded");

    $scope.in = (email, pass) => {
      server.in(email, pass);
      $state.go("admin", { db: "vinos" });
      toaster("Logged in", 2000);
    };

    $scope.hidePass = true;

    $scope.switchPass = () => {
      let pass = document.getElementById("pass");
      $scope.hidePass = $scope.hidePass ? false : true;

      if (pass.type === "password") {
        pass.type = "text";
      } else if (pass.type === "text") {
        pass.type = "password";
      }
    };
  });
