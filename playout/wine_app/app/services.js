app.factory("server", () => {
  let server = {
    auth: firebase.auth(),
    db: firebase.firestore(),
    st: firebase.storage(),
    in: (email, pass) => {
      server.auth.signInWithEmailAndPassword(email, pass).catch(err => console.error(err.message));
    },
    out: () => {
      server.auth.signOut();
      warn("Disconnected");
    },
    user: {}
  };

  server.db.settings({ timestampsInSnapshots: true });

  server.auth.onAuthStateChanged(user => {
    if (user) {
      succ("Connected");
      server.user = user;
    } else {
      warn("Disconnected");
      server.user = null;
    }
  });

  return server;
});
