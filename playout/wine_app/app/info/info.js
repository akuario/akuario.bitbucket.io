angular
  .module("app")
  .controller("infoController", function($scope, server, $state, $stateParams) {
    info("Info loaded");

    let language = $stateParams.lang;
    let selWine = $stateParams.db;
    let wineType;

    $scope.goBack = () => {
      $state.go("list", { lang: language, type: wineType });
    };

    //active database
    server.db
      .collection("vinos")
      .doc(selWine)
      .get()
      .then(doc => {
        $scope.$apply(() => {
          $scope.wine = doc.data();
          succ(`${$scope.wine.name.toUpperCase()} data obtained`);
          wineType = $scope.wine.type;
          $scope.wineType = wineType;
        });
      });

    const uiLang = {
      cas: {
        more: "Más info.",
        marida: "Marida con",
        and: "y",
        desc: "Detalles"
      },
      cat: {
        more: "Més info.",
        marida: "Marida amb",
        and: "i",
        desc: "Detalls"
      },
      eng: {
        more: "More info.",
        marida: "Pairing with",
        and: "and",
        desc: "Details"
      },
      fra: {
        more: "Más info.",
        marida: "Appariement avec",
        and: "et",
        desc: "Détails"
      }
    };

    if (language === "cas") $scope.uiLang = uiLang.cas;
    if (language === "cat") $scope.uiLang = uiLang.cat;
    if (language === "eng") $scope.uiLang = uiLang.eng;
    if (language === "fra") $scope.uiLang = uiLang.fra;
  });
