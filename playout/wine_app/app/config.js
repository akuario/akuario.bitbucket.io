const app = angular.module("app", [
  "oc.lazyLoad",
  "ui.router",
  "angularCSS",
  "ngTouch"
]);

//firebase auth
firebase.initializeApp({
  apiKey: "AIzaSyAGEHpaBX7SrOA2Gb1OBTgQJaWG-5r10lU",
  authDomain: "playout-online.firebaseapp.com",
  databaseURL: "https://playout-online.firebaseio.com",
  projectId: "playout-online",
  storageBucket: "playout-online.appspot.com",
  messagingSenderId: "422965033208"
});

//directives

//on-enter
app.directive("onEnter", () => {
  return (scope, element, attrs) => {
    element.bind("keydown keypress", event => {
      if (event.which === 13) {
        scope.$apply(() => {
          scope.$eval(attrs.onEnter);
        });
        event.preventDefault();
      }
    });
  };
});

//on-esc
app.directive("onEsc", () => {
  return (scope, element, attrs) => {
    element.bind("keydown keypress", event => {
      if (event.which === 27) {
        scope.$apply(() => {
          scope.$eval(attrs.onEsc);
        });
        event.preventDefault();
      }
    });
  };
});
