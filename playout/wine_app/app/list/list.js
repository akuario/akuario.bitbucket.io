angular
  .module("app")
  .controller("listController", function($scope, server, $state, $stateParams) {
    info("List loaded");

    let language = $stateParams.lang;
    let wineType = $stateParams.type;

    $scope.wineType = wineType;

    $scope.goBack = () => {
      $state.go("type", { lang: language });
    };

    $scope.goWine = wine => {
      $state.go("info", {lang: language, db: wine.ref});
    };

    //active database
    let ref = server.db.collection("vinos");
    let query = ref.where("type", "==", wineType);

    query.get().then(snap => {
      let data = [];
      snap.forEach(doc => {
        let values = doc.data();
        values.id = doc.id;
        data.push(values);
      });
      $scope.$apply(() => {
        $scope.wines = data;
        succ(`${wineType.toUpperCase()} WINE data obtained`);
      });
    });

    const uiLang = {
      cas: {
        white: "Vino blanco",
        red: "Vino tinto",
        rose: "Vino rosado",
        spark: "Espumoso"
      },
      cat: {
        white: "Vi blanc",
        red: "Vi negre",
        rose: "Vi rosat",
        spark: "Escumós"
      },
      eng: {
        white: "White wine",
        red: "Red wine",
        rose: "Rose wine",
        spark: "Sparkling"
      },
      fra: {
        white: "Vin blanc",
        red: "Vin rouge",
        rose: "Vin rosé",
        spark: "Vin mousseux"
      }
    };

    if (language === "cas") $scope.uiLang = uiLang.cas;
    if (language === "cat") $scope.uiLang = uiLang.cat;
    if (language === "eng") $scope.uiLang = uiLang.eng;
    if (language === "fra") $scope.uiLang = uiLang.fra;

    if (wineType === "white") $scope.title = $scope.uiLang.white;
    if (wineType === "red") $scope.title = $scope.uiLang.red;
    if (wineType === "rose") $scope.title = $scope.uiLang.rose;
    if (wineType === "spark") $scope.title = $scope.uiLang.spark;
  });
