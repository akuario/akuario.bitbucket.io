angular
  .module("app")
  .controller("typeController", function($scope, server, $stateParams, $state) {
    info("Type loaded");

    let language = $stateParams.lang;
    $scope.whites = 0;
    $scope.reds = 0;
    $scope.roses = 0;
    $scope.sparks = 0;

    //full database
    server.db
      .collection("vinos")
      .get()
      .then(snap => {
        let data = [];
        snap.forEach(doc => {
          let values = doc.data();
          values.id = doc.id;
          data.push(values);
        });
        $scope.$apply(() => {
          $scope.wines = data;
          succ(`VINOS data obtained`);
          data.forEach(el => {
            if (el.type === "white") $scope.whites += 1;
            if (el.type === "red") $scope.reds += 1;
            if (el.type === "rose") $scope.roses += 1;
            if (el.type === "spark") $scope.sparks += 1;
          });
        });
      });

    $scope.goList = t => {
      $state.go("list", {lang: language, type: t});
    }

    const uiLang = {
      cas: {
        white: "Vino blanco",
        red: "Vino tinto",
        rose: "Vino rosado",
        spark: "Espumoso",
        unit: "variedades",
        btn: "Cambiar idioma"
      },
      cat: {
        white: "Vi blanc",
        red: "Vi negre",
        rose: "Vi rosat",
        spark: "Escumós",
        unit: "varietats",
        btn: "Cambiar idioma"
      },
      eng: {
        white: "White wine",
        red: "Red wine",
        rose: "Rose wine",
        spark: "Sparkling",
        unit: "variedades",
        btn: "Change language"
      },
      fra: {
        white: "Vin blanc",
        red: "Vin rouge",
        rose: "Vin rosé",
        spark: "Vin mousseux",
        unit: "variétés",
        btn: "Changer langue"
      }
    };

    if (language === "cas") $scope.uiLang = uiLang.cas;
    if (language === "cat") $scope.uiLang = uiLang.cat;
    if (language === "eng") $scope.uiLang = uiLang.eng;
    if (language === "fra") $scope.uiLang = uiLang.fra;
  });
