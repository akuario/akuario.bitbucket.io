app.config(($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) => {
  $ocLazyLoadProvider.config({
    debug: false,
    events: false
  });

  $urlRouterProvider.otherwise("/lang");

  $stateProvider.state("lang", {
    name: "lang",
    url: "/lang",
    views: {
      content: {
        templateUrl: "./app/lang/lang.html",
        controller: "langController"
      }
    },
    resolve: {
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/lang.css"
          },
          {
            href: "./assets/css/m.lang.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/lang.js");
      }
    }
  });

  $stateProvider.state("type", {
    name: "type",
    url: "/type/:lang",
    params: { lang: null },
    views: {
      content: {
        templateUrl: "./app/type/type.html",
        controller: "typeController"
      }
    },
    resolve: {
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/type.css"
          },
          {
            href: "./assets/css/m.type.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/type.js");
      }
    }
  });
  
  $stateProvider.state("list", {
    name: "list",
    url: "/list/:lang/:type",
    params: { lang: null, type: null},
    views: {
      content: {
        templateUrl: "./app/list/list.html",
        controller: "listController"
      }
    },
    resolve: {
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/list.css"
          },
          {
            href: "./assets/css/m.list.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/list.js");
      }
    }
  });
  
  $stateProvider.state("info", {
    name: "info",
    url: "/info/:lang/:db",
    params: { lang: null, db: null },
    views: {
      content: {
        templateUrl: "./app/info/info.html",
        controller: "infoController"
      }
    },
    resolve: {
      loadCss: $css => {
        return $css.add([
          {
            href: "./assets/css/info.css"
          },
          {
            href: "./assets/css/m.info.css",
            media: "screen and (max-width : 768px)"
          }
        ]);
      },
      contentCtrl: $ocLazyLoad => {
        return $ocLazyLoad.load("./assets/js/info.js");
      }
    }
  });
});
