angular.module("app").controller("langController", function($scope, $state) {
  info("Lang loaded");

  $scope.goType = l => {
    $state.go("type", { lang: l});
  }
});
