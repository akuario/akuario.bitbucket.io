const app = document.getElementById("app");
const canvas = document.createElement("canvas");
const draw = canvas.getContext("2d");
const s = 20;
let speed = 7;

canvas.width = window.innerWidth - 20;
canvas.height = window.innerHeight - 20;

window.onresize = () => {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
};

app.appendChild(canvas);

let snake = new Snake();

window.addEventListener("keydown", e => {
  if (e.key === "ArrowUp") {
    snake.dir(0, -1);
  } else if (e.key === "ArrowRight") {
    snake.dir(1, 0);
  } else if (e.key === "ArrowDown") {
    snake.dir(0, 1);
  } else if (e.key === "ArrowLeft") {
    snake.dir(-1, 0);
  }
});

let hammer = new Hammer(window);

hammer.on("hammer.input", e => {
  if (e.direction === 8) {
    snake.dir(0, -1);
  } else if (e.direction === 4) {
    snake.dir(1, 0);
  } else if (e.direction === 16) {
    snake.dir(0, 1);
  } else if (e.direction === 2) {
    snake.dir(-1, 0);
  }
});

setInterval(() => {
  draw.clearRect(0, 0, canvas.width, canvas.height);

  snake.update();
  snake.show();
}, 1000 / speed);
