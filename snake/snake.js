class Snake {
  constructor() {
    this.x = 0;
    this.y = 0;
    this.xspeed = 1;
    this.yspeed = 0;
    this.img = document.createElement("img");
    this.img.src = "./assets/slime.png";
  }

  dir(x, y) {
    if (this.xspeed > 0 && x !== 0) {
      x = 1;
    } else if (this.xspeed < 0 && x !== 0) {
      x = -1;
    }

    if (this.yspeed > 0 && y !== 0) {
      y = 1;
    } else if (this.yspeed < 0 && y !== 0) {
      y = -1;
    }
    this.xspeed = x;
    this.yspeed = y;
  }

  update() {
    if (this.x < 0) {
      this.x = canvas.width;
      this.x = this.x + this.xspeed * s;
    } else if (this.x > canvas.width - s) {
      this.x = 0;
      this.x = this.x + this.xspeed * s;
    } else {
      this.x = this.x + this.xspeed * s;
    }

    if (this.y < 0) {
      this.y = canvas.height;
      this.y = this.y + this.yspeed * s;
    } else if (this.y > canvas.height - s) {
      this.y = 0;
      this.y = this.y + this.yspeed * s;
    } else {
      this.y = this.y + this.yspeed * s;
    }
  }

  show() {
    draw.drawImage(this.img, this.x, this.y, s*2, s*2);
  }
}
