angular.module("app").controller("navController", function($scope) {
  info("Nav loaded");

  $scope.mode = () => {
    if (localStorage.getItem("modo") === "oscuro") {
      return "claro";
    } else {
      return "oscuro";
    }
  };

  $scope.switch = () => {
    _(".switch-label").tog("active");
    _(".home-box").tog("light");
    _("nav").tog("nav-light");

    $scope.mode = () => {
      if (localStorage.getItem("modo") === "oscuro") {
        return "claro";
      } else {
        return "oscuro";
      }
    };
    let mode = localStorage.getItem("modo");

    localStorage.removeItem("modo");
    if (mode === "oscuro") {
      localStorage.setItem("modo", "claro");
    }
    if (mode === "claro") {
      localStorage.setItem("modo", "oscuro");
    }
  };
});
