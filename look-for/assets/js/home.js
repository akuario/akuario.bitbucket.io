angular.module("app").controller("homeController", function($scope, $interval) {
  info("Home loaded");

  $scope.switch = () => {
    _(".switch-label").tog("active");
    _(".home-box").tog("light");
    _("nav").tog("nav-light");

    let mode = localStorage.getItem("modo");

    localStorage.removeItem("modo");
    if (mode === "oscuro") {
      localStorage.setItem("modo", "claro");
    }
    if (mode === "claro") {
      localStorage.setItem("modo", "oscuro");
    }
  };

  if (localStorage.length === 0) {
    localStorage.setItem("modo", "oscuro");
  }

  if (localStorage.modo === "oscuro") {
    _(".switch-label").del("active");
    _(".home-box").del("light");
    _("nav").del("nav-light");
  }
  if (localStorage.modo === "claro") {
    _(".switch-label").add("active");
    _(".home-box").add("light");
    _("nav").add("nav-light");
  }

  //focus input on page load
  _("input").focus();

  //declare keyword variable
  $scope.keyword = "";

  //select google on page load
  $scope.selected = () => $scope.google();
  _("#google").add("selected");

  //search engines

  //search
  $scope.google = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.google();
      _("li").del("selected");
      _("#google").add("selected");
    } else {
      window.location = `https://www.google.es/search?&q=${$scope.keyword}`;
    }
    _("input").focus();
  };

  //images
  $scope.gImages = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.gImages();
      _("li").del("selected");
      _("#gImages").add("selected");
    } else {
      window.location = `https://www.google.es/search?tbm=isch&q=${
        $scope.keyword
      }`;
    }
    _("input").focus();
  };

  $scope.unsplash = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.unsplash();
      _("li").del("selected");
      _("#unsplash").add("selected");
    } else {
      window.location = `https://unsplash.com/search/photos/${$scope.keyword}`;
    }
    _("input").focus();
  };

  $scope.pexels = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.pexels();
      _("li").del("selected");
      _("#pexels").add("selected");
    } else {
      window.location = `https://www.pexels.com/search/${$scope.keyword}`;
    }
    _("input").focus();
  };

  //vectors
  $scope.freepik = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.freepik();
      _("li").del("selected");
      _("#freepik").add("selected");
    } else {
      window.location = `https://www.freepik.es/search?dates=any&format=search&page=1&query=${
        $scope.keyword
      }&sort=popular`;
    }
    _("input").focus();
  };

  $scope.flaticon = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.flaticon();
      _("li").del("selected");
      _("#flaticon").add("selected");
    } else {
      window.location = `https://www.flaticon.com/search?word=${
        $scope.keyword
      }`;
    }
    _("input").focus();
  };

  //png
  $scope.kisspng = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.kisspng();
      _("li").del("selected");
      _("#kisspng").add("selected");
    } else {
      window.location = `https://www.kisspng.com/free/${$scope.keyword}.html`;
    }
    _("input").focus();
  };

  $scope.pngtree = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.pngtree();
      _("li").del("selected");
      _("#pngtree").add("selected");
    } else {
      window.location = `https://pngtree.com/so/${$scope.keyword}`;
    }
    _("input").focus();
  };

  //types
  $scope.gFonts = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.gFonts();
      _("li").del("selected");
      _("#gFonts").add("selected");
    } else {
      window.location = `https://fonts.google.com/?query=${$scope.keyword}`;
    }
    _("input").focus();
  };

  $scope.dafont = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.dafont();
      _("li").del("selected");
      _("#dafont").add("selected");
    } else {
      window.location = `https://www.dafont.com/es/search.php?q=${
        $scope.keyword
      }`;
    }
    _("input").focus();
  };

  $scope.fontspace = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.fontspace();
      _("li").del("selected");
      _("#fontspace").add("selected");
    } else {
      window.location = `https://www.fontspace.com/search/?q=${$scope.keyword}`;
    }
    _("input").focus();
  };

  //videos
  $scope.youtube = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.youtube();
      _("li").del("selected");
      _("#youtube").add("selected");
    } else {
      window.location = `https://www.youtube.com/results?search_query=${
        $scope.keyword
      }`;
    }
    _("input").focus();
  };

  $scope.videezy = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.videezy();
      _("li").del("selected");
      _("#videezy").add("selected");
    } else {
      window.location = `https://es.videezy.com/video-gratis/${$scope.keyword}`;
    }
    _("input").focus();
  };

  $scope.pVideos = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.pVideos();
      _("li").del("selected");
      _("#pVideos").add("selected");
    } else {
      window.location = `https://www.pexels.com/search/videos/${
        $scope.keyword
      }`;
    }
    _("input").focus();
  };

  //inspiration
  $scope.pinterest = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.pinterest();
      _("li").del("selected");
      _("#pinterest").add("selected");
    } else {
      window.location = `https://www.pinterest.es/search/pins/?q=${
        $scope.keyword
      }`;
    }
    _("input").focus();
  };

  $scope.dribbble = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.dribbble();
      _("li").del("selected");
      _("#dribbble").add("selected");
    } else {
      window.location = `https://dribbble.com/search?q=${$scope.keyword}`;
    }
    _("input").focus();
  };

  $scope.behance = () => {
    if ($scope.keyword === undefined || $scope.keyword === "") {
      $scope.selected = () => $scope.behance();
      _("li").del("selected");
      _("#behance").add("selected");
    } else {
      window.location = `https://www.behance.net/search?content=projects&sort=appreciations&time=week&featured_on_behance=true&search=${
        $scope.keyword
      }`;
    }
    _("input").focus();
  };

  //multiple search engines
  $scope.allImages = () => {
    if ($scope.keyword !== undefined) {
      window.open(
        `https://www.google.es/search?&q=${$scope.keyword}`,
        "_blank"
      );
      window.open(
        `https://unsplash.com/search/photos/${$scope.keyword}`,
        "_blank"
      );
      window.open(`https://www.pexels.com/search/${$scope.keyword}`, "_blank");
    }
    $scope.keyword = undefined;
    _("input").focus();
  };

  $scope.allVectors = () => {
    if ($scope.keyword !== undefined) {
      window.location = `https://www.freepik.es/search?dates=any&format=search&page=1&query=${
        $scope.keyword
      }&sort=popular`;
      window.open(
        `https://www.flaticon.com/search?word=${$scope.keyword}`,
        "_blank"
      );
    }
    $scope.keyword = undefined;
    _("input").focus();
  };

  $scope.allPng = () => {
    if ($scope.keyword !== undefined) {
      window.location = `https://www.kisspng.com/free/${$scope.keyword}.html`;
      window.open(`https://pngtree.com/so/${$scope.keyword}`, "_blank");
    }
    $scope.keyword = undefined;
    _("input").focus();
  };

  $scope.allTypes = () => {
    if ($scope.keyword !== undefined) {
      window.open(
        `https://fonts.google.com/?query=${$scope.keyword}`,
        "_blank"
      );
      window.open(
        `https://www.dafont.com/es/search.php?q=${$scope.keyword}`,
        "_blank"
      );
      window.open(
        `https://www.fontspace.com/search/?q=${$scope.keyword}`,
        "_blank"
      );
    }
    $scope.keyword = undefined;
    _("input").focus();
  };

  $scope.allVideos = () => {
    if ($scope.keyword !== undefined) {
      window.open(
        `https://www.youtube.com/results?search_query=${$scope.keyword}`,
        "_blank"
      );
      window.open(
        `https://es.videezy.com/video-gratis/${$scope.keyword}`,
        "_blank"
      );
      window.open(
        `https://www.pexels.com/search/videos/${$scope.keyword}`,
        "_blank"
      );
    }
    $scope.keyword = undefined;
    _("input").focus();
  };

  $scope.allInspiration = () => {
    if ($scope.keyword !== undefined) {
      window.open(
        `https://www.pinterest.es/search/pins/?q=${$scope.keyword}`,
        "_blank"
      );
      window.open(`https://dribbble.com/search?q=${$scope.keyword}`, "_blank");
      window.open(
        `https://www.behance.net/search?content=projects&sort=appreciations&time=week&featured_on_behance=true&search=${
          $scope.keyword
        }`,
        "_blank"
      );
    }
    $scope.keyword = undefined;
    _("input").focus();
  };

  //shortkeys
  let map = {};
  $scope.shortKey = e => {
    map[e.keyCode] = e.type == "keydown";

    let control = map[17];
    let shift = map[16];
    let space = map[32];
    let key1 = map[49];
    let key2 = map[50];
    let key3 = map[51];
    let key4 = map[52];
    let key5 = map[53];
    let key6 = map[54];
    let key7 = map[55];
    let key8 = map[56];
    let key9 = map[57];
    let key0 = map[48];
    let keyA = map[65];
    let keyH = map[72];
    let keyK = map[75];
    let keyL = map[76];
    let keyE = map[69];
    let keyF = map[70];
    let keyQ = map[81];
    let keyS = map[83];

    //google
    if (control && shift && key1) {
      $scope.google();
      map = {};
    }
    //gImages
    if (control && shift && key3) {
      $scope.gImages();
      map = {};
    }
    //unsplash
    if (control && shift && key4) {
      $scope.unsplash();
      map = {};
    }
    //pexels
    if (control && shift && key5) {
      $scope.pexels();
      map = {};
    }
    //freepik
    if (control && shift && key6) {
      $scope.freepik();
      map = {};
    }
    //flaticon
    if (control && shift && key7) {
      $scope.flaticon();
      map = {};
    }
    //kisspng
    if (control && shift && keyQ) {
      $scope.kisspng();
      map = {};
    }
    //pngtree
    if (control && shift && keyS) {
      $scope.pngtree();
      map = {};
    }
    //gFonts
    if (control && shift && key8) {
      $scope.gFonts();
      map = {};
    }
    //dafont
    if (control && shift && key9) {
      $scope.dafont();
      map = {};
    }
    //fontspace
    if (control && shift && key0) {
      $scope.fontspace();
      map = {};
    }
    //youtube
    if (control && shift && keyA) {
      $scope.youtube();
      map = {};
    }
    //videezy
    if (control && shift && keyH) {
      $scope.videezy();
      map = {};
    }
    //pVideos
    if (control && shift && keyK) {
      $scope.pVideos();
      map = {};
    }
    //pinterest
    if (control && shift && keyL) {
      $scope.pinterest();
      map = {};
    }
    //dribbble
    if (control && shift && keyE) {
      $scope.dribbble();
      map = {};
    }
    //behance
    if (control && shift && keyF) {
      $scope.behance();
      map = {};
    }
  };

  const speechRecognition =
    window.SpeechRecognition || window.webkitSpeechRecognition;
  const recognition = new speechRecognition();

  recognition.onstart = () => {
    log("Listening...");
  };

  recognition.onresult = e => {
    const current = e.resultIndex;
    const transcript = e.results[current][0].transcript;
    log(transcript);

    if (transcript.includes("buscar en")) {
      readOut(transcript);
    }
  };

  recognition.onend = () => {
    recognition.start();
  };

  recognition.start();

  speechSynthesis.getVoices();

  function readOut(m) {
    const speech = new SpeechSynthesisUtterance();
    speech.volume = 1;
    speech.rate = 1.1;
    speech.pitch = 1.1;
    speech.voice = speechSynthesis.getVoices()[6];
    speech.text = "Lo siento, solo puedo responder a comandos de voz.";

    if (m.includes("modo oscuro")) {
      $scope.switch();
      speech.text = "";
    }
    if (m.includes("modo claro")) {
      $scope.switch();
      speech.text = "";
    }

    //web
    if (m.includes("buscar en Google")) {
      const q = m.replace("buscar en Google ", "");

      $scope.keyword = q;
      $scope.google();
      speech.text = "";
    }

    //images
    if (m.includes("buscar en Google Imágenes")) {
      const q = m.replace("buscar en Google Imágenes ", "");

      $scope.keyword = q;
      $scope.gImages();
      speech.text = "";
    }
    if (m.includes("buscar en un splash")) {
      const q = m.replace("buscar en un splash ", "");

      $scope.keyword = q;
      $scope.unsplash();
      speech.text = "";
    }
    if (m.includes("buscar en pexels")) {
      const q = m.replace("buscar en pexels ", "");

      $scope.keyword = q;
      $scope.pexels();
      speech.text = "";
    }
    if (m.includes("buscar en todas las imágenes")) {
      const q = m.replace("buscar en todas las imágenes ", "");

      $scope.keyword = q;
      $scope.allImages();
      speech.text = "";
    }

    //vectors
    if (m.includes("buscar en Freepik")) {
      const q = m.replace("buscar en Freepik ", "");

      $scope.keyword = q;
      $scope.freepik();
      speech.text = "";
    }
    if (m.includes("buscar en flat icon")) {
      const q = m.replace("buscar en flat icon ", "");

      $scope.keyword = q;
      $scope.flaticon();
      speech.text = "";
    }
    if (m.includes("buscar en todos los vectores")) {
      const q = m.replace("buscar en todos los vectores ", "");

      $scope.keyword = q;
      $scope.allVectors();
      speech.text = "";
    }

    //png
    if (m.includes("buscar en Kiss PNG")) {
      const q = m.replace("buscar en Kiss PNG ", "");

      $scope.keyword = q;
      $scope.kisspng();
      speech.text = "";
    }
    if (m.includes("buscar en PNG tree")) {
      const q = m.replace("buscar en PNG tree ", "");

      $scope.keyword = q;
      $scope.pngtree();
      speech.text = "";
    }
    if (m.includes("buscar en todos los PNG")) {
      const q = m.replace("buscar en todos los PNG ", "");

      $scope.keyword = q;
      $scope.allPng();
      speech.text = "";
    }

    //types
    if (m.includes("buscar en Google Fonts")) {
      const q = m.replace("buscar en Google Fonts ", "");

      $scope.keyword = q;
      $scope.gFonts();
      speech.text = "";
    }
    if (m.includes("buscar en Dafont")) {
      const q = m.replace("buscar en Dafont ", "");

      $scope.keyword = q;
      $scope.dafont();
      speech.text = "";
    }
    if (m.includes("buscar en fontspace")) {
      const q = m.replace("buscar en fontspace ", "");

      $scope.keyword = q;
      $scope.fontspace();
      speech.text = "";
    }
    if (m.includes("buscar en todas las tipografías")) {
      const q = m.replace("buscar en todas las tipografías ", "");

      $scope.keyword = q;
      $scope.allTypes();
      speech.text = "";
    }

    //videos
    if (m.includes("buscar en youtube")) {
      const q = m.replace("buscar en youtube ", "");

      $scope.keyword = q;
      $scope.youtube();
      speech.text = "";
    }
    if (m.includes("buscar en videezy")) {
      const q = m.replace("buscar en videezy ", "");

      $scope.keyword = q;
      $scope.videezy();
      speech.text = "";
    }
    if (m.includes("buscar en pexels vídeo")) {
      const q = m.replace("buscar en pexels vídeo ", "");

      $scope.keyword = q;
      $scope.pVideos();
      speech.text = "";
    }
    if (m.includes("buscar en todos los vídeos")) {
      const q = m.replace("buscar en todos los vídeos ", "");

      $scope.keyword = q;
      $scope.allVideos();
      speech.text = "";
    }

    //inspiration
    if (m.includes("buscar en Pinterest")) {
      const q = m.replace("buscar en Pinterest ", "");

      $scope.keyword = q;
      $scope.pinterest();
      speech.text = "";
    }
    if (m.includes("buscar en dribbble")) {
      const q = m.replace("buscar en dribbble ", "");

      $scope.keyword = q;
      $scope.dribbble();
      speech.text = "";
    }
    if (m.includes("buscar en Drive")) {
      const q = m.replace("buscar en Drive ", "");

      $scope.keyword = q;
      $scope.dribbble();
      speech.text = "";
    }
    if (m.includes("buscar en driver")) {
      const q = m.replace("buscar en driver ", "");

      $scope.keyword = q;
      $scope.dribbble();
      speech.text = "";
    }
    if (m.includes("buscar en Behance")) {
      const q = m.replace("buscar en Behance ", "");

      $scope.keyword = q;
      $scope.behance();
      speech.text = "";
    }
    if (m.includes("buscar en toda la inspiración")) {
      const q = m.replace("buscar en toda la inspiración ", "");

      $scope.keyword = q;
      $scope.allInspiration();
      speech.text = "";
    }

    window.speechSynthesis.speak(speech);
  }
});


//# sourceMappingURL=home.js.map