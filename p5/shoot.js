class Shoot {
  constructor(x, y, axys) {
    this.x = x;
    this.y = y;
    this.vel = unit;
    this.axys = axys;
  }

  show() {
    noStroke();
    fill(255, 0, 0);
    rect(this.x, this.y, unit / 4, unit / 4);
  }

  fire() {
    if (this.axys === "up") {
      this.x += 0;
      this.y += -this.vel;
    }
    if (this.axys === "up_right") {
      this.x += this.vel;
      this.y += -this.vel;
    }
    if (this.axys === "right") {
      this.x += this.vel;
      this.y += 0;
    }
    if (this.axys === "down_right") {
      this.x += this.vel;
      this.y += this.vel;
    }
    if (this.axys === "down") {
      this.x += 0;
      this.y += this.vel;
    }
    if (this.axys === "down_left") {
      this.x += -this.vel;
      this.y += this.vel;
    }
    if (this.axys === "left") {
      this.x += -this.vel;
      this.y += 0;
    }
    if (this.axys === "up_left") {
      this.x += -this.vel;
      this.y += -this.vel;
    }

    for (let j = 0; j < bots.length; j++) {
      if (dist(this.x, this.y, bots[j].x, bots[j].y) < unit) {
        bots.splice(j, 1);
        this.x = -width * 2;
        this.y = -height * 2;
      }
    }
  }
}
