let wW = window.innerWidth;
let wH = window.innerHeight;

let unit;
let unitY;

(function responsive() {
  if (wW < 1024) {
    unit = wW / 25;
  } else {
    unit = wW / 50;
  }
})();

let hammer = new Hammer(window);

let player;
let bot;
let bullets = [];
let bots = [];


function genBots(n) {
  for (let i = 0; i < 10; i++) {
    bot = new Player();
    bot.x = random(0, width - unit);
    bot.y = random(0, height - unit);
    bots.push(bot);
  }
}

function setup() {
  createCanvas(wW, wH);
  player = new Player();

  genBots(50);

  hammer.on("hammer.input", e => {
    if (e.direction === 8) {
      player.move(0, -1);
    } else if (e.direction === 4) {
      player.move(1, 0);
    } else if (e.direction === 16) {
      player.move(0, 1);
    } else if (e.direction === 2) {
      player.move(-1, 0);
    }
  });
}

function draw() {
  background(0);

  if (bots.length === 0) {
    genBots(50)
  }

  for (let i = 0; i < bots.length; i++) {
    bots[i].show();
    bots[floor(random(0, bots.length - 1))].move(randomGaussian(random(-1, 1)), randomGaussian(random(-1, 1)));
  }

  for (let i = 0; i < bullets.length; i++) {
    bullets[i].show();
    bullets[i].fire();

    if (bullets[i].x >= width) {
      bullets.splice(i, 1);
    } else if (bullets[i].x <= 0 - unit) {
      bullets.splice(i, 1);
    } else if (bullets[i].y >= height) {
      bullets.splice(i, 1);
    } else if (bullets[i].y <= 0 - unit) {
      bullets.splice(i, 1);
    }
  }

  player.show();

  noStroke();
  fill(0, 255, 0);

  controls();
}

hammer.on("tap", () => {
  bullets.push(new Shoot(player.x + unit / 2, player.y + unit / 2, player.axys));
})

function keyPressed() {
  if (key === " ") {
    bullets.push(new Shoot(player.x + unit / 2, player.y + unit / 2, player.axys));
  }
}
