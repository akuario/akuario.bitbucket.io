class Player {
  constructor() {
    this.x = width / 2;
    this.y = height / 2;
    this.vel = 3;
    this.digVel = this.vel + 1.5;
    this.up = loadImage("./assets/img/up.png");
    this.right = loadImage("./assets/img/right.png");
    this.down = loadImage("./assets/img/down.png");
    this.left = loadImage("./assets/img/left.png");
    this.up_right = loadImage("./assets/img/up_right.png");
    this.down_right = loadImage("./assets/img/down_right.png");
    this.down_left = loadImage("./assets/img/down_left.png");
    this.up_left = loadImage("./assets/img/up_left.png");
    this.pos = this.down;
    this.axys = "down";
  }

  show() {
    noStroke();
    fill(255);
    image(this.pos, this.x, this.y, unit, unit);
  }

  move(x, y) {
    if (x !== 0 && y !== 0) {
      this.x += (x * unit) / 2 / this.digVel;
      this.y += (y * unit) / 2 / this.digVel;
    } else {
      this.x += (x * unit) / 2 / this.vel;
      this.y += (y * unit) / 2 / this.vel;
    }

    if (x === 0 && y === -1) {
      this.pos = this.up;
      this.axys = "up";
    } else if (x === 1 && y === -1) {
      this.pos = this.up_right;
      this.axys = "up_right";
    } else if (x === 1 && y === 0) {
      this.pos = this.right;
      this.axys = "right";
    } else if (x === 1 && y === 1) {
      this.pos = this.down_right;
      this.axys = "down_right";
    } else if (x === 0 && y === 1) {
      this.pos = this.down;
      this.axys = "down";
    } else if (x === -1 && y === 1) {
      this.pos = this.down_left;
      this.axys = "down_left";
    } else if (x === -1 && y === 0) {
      this.pos = this.left;
      this.axys = "left";
    } else if (x === -1 && y === -1) {
      this.pos = this.up_left;
      this.axys = "up_left";
    }

    //limits
    if (this.x >= width - unit) {
      this.x = width - unit;
    } else if (this.x <= 0) {
      this.x = 0;
    } else if (this.y >= height - unit) {
      this.y = height - unit;
    } else if (this.y <= 0) {
      this.y = 0;
    }
  }
}
