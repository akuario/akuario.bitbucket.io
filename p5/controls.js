function controls() {
  //linear moves
  if (
    keyIsDown(UP_ARROW) &&
    !keyIsDown(RIGHT_ARROW) &&
    !keyIsDown(DOWN_ARROW) &&
    !keyIsDown(LEFT_ARROW)
  ) {
    player.move(0, -1);
  } else if (
    !keyIsDown(UP_ARROW) &&
    keyIsDown(RIGHT_ARROW) &&
    !keyIsDown(DOWN_ARROW) &&
    !keyIsDown(LEFT_ARROW)
  ) {
    player.move(1, 0);
  } else if (
    !keyIsDown(UP_ARROW) &&
    !keyIsDown(RIGHT_ARROW) &&
    keyIsDown(DOWN_ARROW) &&
    !keyIsDown(LEFT_ARROW)
  ) {
    player.move(0, 1);
  } else if (
    !keyIsDown(UP_ARROW) &&
    !keyIsDown(RIGHT_ARROW) &&
    !keyIsDown(DOWN_ARROW) &&
    keyIsDown(LEFT_ARROW)
  ) {
    player.move(-1, 0);
  }

  //combo moves
  if (
    keyIsDown(UP_ARROW) &&
    keyIsDown(RIGHT_ARROW) &&
    !keyIsDown(DOWN_ARROW) &&
    !keyIsDown(LEFT_ARROW)
  ) {
    player.move(1, -1);
  } else if (
    !keyIsDown(UP_ARROW) &&
    keyIsDown(RIGHT_ARROW) &&
    keyIsDown(DOWN_ARROW) &&
    !keyIsDown(LEFT_ARROW)
  ) {
    player.move(1, 1);
  } else if (
    !keyIsDown(UP_ARROW) &&
    !keyIsDown(RIGHT_ARROW) &&
    keyIsDown(DOWN_ARROW) &&
    keyIsDown(LEFT_ARROW)
  ) {
    player.move(-1, 1);
  } else if (
    keyIsDown(UP_ARROW) &&
    !keyIsDown(RIGHT_ARROW) &&
    !keyIsDown(DOWN_ARROW) &&
    keyIsDown(LEFT_ARROW)
  ) {
    player.move(-1, -1);
  }
}